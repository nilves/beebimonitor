/**
 * Parse and execute commands send over websocket
 */

#ifndef __cmd_h__
#define __cmd_h__

#include <Arduino.h>
#include <WebSocketsServer.h>

//also defined in main.cpp
#define BUILT_IN_LED (4)

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length);

void registerCmd();

bool processCmd(const char *cmd, char *rsp);

#endif