#include <Arduino.h>
#include <WiFi.h>
#include <esp_now.h>

// esp8266 mac address EC:FA:BC:0E:B6:55
uint8_t broadcastAddress1[] = {0xEC, 0xFA, 0xBC, 0x0E, 0xB6, 0x55};
// espcam mac address 7C:9E:BD:38:40:D8
uint8_t broadcastAddress2[] = {0x7C, 0x9E, 0xBD, 0x38, 0x40, 0xD8};

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  //memcpy(&incomingReadings, incomingData, sizeof(incomingReadings));
  Serial.print("Bytes received: ");
  Serial.println(len);
  
  Serial.print("data:\"");
  Serial.print((char*) incomingData);
  Serial.println("\"");

  //incomingTemp = incomingReadings.temp;
  //incomingHum = incomingReadings.hum;
  //incomingPres = incomingReadings.pres;
}


void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);
  WiFi.mode(WIFI_MODE_STA);
  
  Serial.println("MAC address:");
  Serial.println(WiFi.macAddress());

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);

  // Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress1, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);
}

void loop() {
  // put your main code here, to run repeatedly:

  // Send message via ESP-NOW
  char tmp_data[] = "Hello world! from esp32 cam.";

  esp_err_t result = esp_now_send(broadcastAddress1, (uint8_t *) &tmp_data, sizeof(tmp_data));
   
  if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else {
    Serial.println("Error sending the data");
  }

  //delay(1000);
}