#include <cmd.h>
#include <CommandParser.h>
#include <Arduino.h>
#include <Audio.h>
#include <Noise.h>



extern Audio *audio;
extern NoiseGenerator *noise;
extern WebSocketsServer webSocket;

//send the audiodata data over websocket
void sendAudio(uint8_t * payload, size_t length)
{
  //Serial.print("+.");
  webSocket.broadcastBIN(payload, length);
}

void dummy(uint8_t * payload, size_t length){
  //intenitonally blank
}



#define SET_MAX_COMMANDS (16)
#define SET_MAX_COMMAND_ARGS (2)
#define SET_MAX_COMMAND_NAME_LENGTH (16)
#define SET_MAX_COMMAND_ARG_SIZE (32)
#define SET_MAX_RESPONSE_SIZE (64)

typedef CommandParser<SET_MAX_COMMANDS, SET_MAX_COMMAND_ARGS, SET_MAX_COMMAND_NAME_LENGTH, SET_MAX_COMMAND_ARG_SIZE, SET_MAX_RESPONSE_SIZE> MyCommandParser;

MyCommandParser parser;


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            Serial.printf("[%u] Disconnected!\n", num);
            break;
        case WStype_CONNECTED:
            {
                IPAddress ip = webSocket.remoteIP(num);
                Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);

				        // send message to client
				        webSocket.sendTXT(num, "Connected");
            }
            break;
        case WStype_TEXT:
            {
              Serial.printf("[%u] got cmd: %s\n", num, payload);

              char response[SET_MAX_RESPONSE_SIZE];
              processCmd((char*)payload, response);

              webSocket.sendTXT(num, response);
            }
            break;
        case WStype_BIN:
            Serial.printf("[%u] get binary length: %u\n", num, length);
            audio->playFrames(payload, length);
            break;
        case WStype_ERROR:			
        case WStype_FRAGMENT_TEXT_START:
        case WStype_FRAGMENT_BIN_START:
        case WStype_FRAGMENT:
        case WStype_FRAGMENT_FIN:
        case WStype_PING:
        case WStype_PONG:
          break;
    }

}


/** These functions handle the commands */
void Reboot_handler(MyCommandParser::Argument *args, char *response) {
  //TODO
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void RecordAlways_handler(MyCommandParser::Argument *args, char *response) {
  // make the mic callback send audio over the websocket
  audio->onMicDataAvailable(sendAudio);

  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void RecordPeak_handler(MyCommandParser::Argument *args, char *response) {
  // make the mic callback send audio over the websocket only when audio level is over the threshold send
  uint64_t threshold = args[0].asUInt64;
  audio->onMicDataAvailable(sendAudio);

    //TODO 

  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void RecordOff_handler(MyCommandParser::Argument *args, char *response) {
  // do not send mic data over websocket => set dummy mic callback
  audio->onMicDataAvailable(dummy);
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void PlayNoise_handler(MyCommandParser::Argument *args, char *response) {
  // set websocket handler to drop any recieved audio data
  audio->setMuxPlayNoise();
  // set start the noise handler
  noise->start();
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void SetNoiseLevel_handler(MyCommandParser::Argument *args, char *response) {
  // set the noise level 
  uint8_t volume = (uint8_t)args[0].asUInt64;
  noise->set_volume(volume);
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void PlayAudio_handler(MyCommandParser::Argument *args, char *response) {
  // set websocket handler to play any recieved audio data
  audio->setMuxPlayFrames();
  // stop the noise generator
  noise->stop();
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void SetAudioVolume_handler(MyCommandParser::Argument *args, char *response) {
  // set the audio volume 
  uint8_t volume = (uint8_t)args[0].asUInt64;
    //TODO
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void PlayOff_handler(MyCommandParser::Argument *args, char *response) {
  // set websocket handler to to drop any recieved audio data
  audio->setMuxOff();
  // stop the noise generator
  noise->stop();
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void LampOn_handler(MyCommandParser::Argument *args, char *response) {
  // Turn on lamp
  digitalWrite(BUILT_IN_LED, LOW);
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void SetLampLevel_handler(MyCommandParser::Argument *args, char *response) {
  uint8_t level = (uint8_t)args[0].asUInt64;
  // set lamp level
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void LampOff_handler(MyCommandParser::Argument *args, char *response) {
  // turn off lamp
  digitalWrite(BUILT_IN_LED, HIGH);
  strlcpy(response, "OK", MyCommandParser::MAX_RESPONSE_SIZE);
}

void registerCmd(){
    parser.registerCommand("Reboot", "", &Reboot_handler);
    parser.registerCommand("RecordAlways", "", &RecordAlways_handler);
    parser.registerCommand("RecordPeak", "u", &RecordPeak_handler);
    parser.registerCommand("RecordOff", "", &RecordOff_handler);
    parser.registerCommand("PlayNoise", "", &PlayNoise_handler);
    parser.registerCommand("SetNoiseLevel", "u", &SetNoiseLevel_handler);
    parser.registerCommand("PlayAudio", "", &PlayAudio_handler);
    parser.registerCommand("SetAudioVolume", "u", &SetAudioVolume_handler);
    parser.registerCommand("PlayOff", "", &PlayOff_handler);
    parser.registerCommand("LampOn", "", &LampOn_handler);
    parser.registerCommand("SetLampLevel", "u", &SetLampLevel_handler);
    parser.registerCommand("LampOff", "", &LampOff_handler);
    /** Todo commands
     * GetTemp1
     * GetTemp2
     */
}

bool processCmd(const char *cmd, char *rsp){
    return parser.processCommand(cmd, rsp);
}


/* old example stuff
void cmd_test(MyCommandParser::Argument *args, char *response) {
  Serial.print("string: "); Serial.println(args[0].asString);
  Serial.print("double: "); Serial.println(args[1].asDouble);
  Serial.print("int64: "); Serial.println((int32_t)args[2].asInt64); // NOTE: on older AVR-based boards, Serial doesn't support printing 64-bit values, so we'll cast it down to 32-bit
  Serial.print("uint64: "); Serial.println((uint32_t)args[3].asUInt64); // NOTE: on older AVR-based boards, Serial doesn't support printing 64-bit values, so we'll cast it down to 32-bit
  strlcpy(response, "success", MyCommandParser::MAX_RESPONSE_SIZE);
}

void setup() {
  Serial.begin(9600);
  while (!Serial);

  parser.registerCommand("TEST", "sdiu", &cmd_test);
  Serial.println("registered command: TEST <string> <double> <int64> <uint64>");
  Serial.println("example: TEST \"\\x41bc\\ndef\" -1.234e5 -123 123");
}

void loop() {
  if (Serial.available()) {
    char line[128];
    size_t lineLength = Serial.readBytesUntil('\n', line, 127);
    line[lineLength] = '\0';

    char response[MyCommandParser::MAX_RESPONSE_SIZE];
    parser.processCommand(line, response);
    Serial.println(response);
  }
}
*/