#include <Camera_srv.h>
#include <Arduino.h>

httpd_handle_t s_httpd = NULL;

esp_err_t jpg_httpd_handler(httpd_req_t *req)
{
    //Serial.println("httpd_handler started!");

    camera_fb_t *fb = NULL;
    esp_err_t res = ESP_OK;
    size_t fb_len = 0;
    //int64_t fr_start = esp_timer_get_time();

    fb = esp_camera_fb_get();
    if (!fb)
    {
        Serial.println("Camera capture failed");
        httpd_resp_send_500(req);
        return ESP_FAIL;
    }
    
    res = httpd_resp_set_type(req, "image/jpeg");

    if (res == ESP_OK)
    {
        fb_len = fb->len;
        res = httpd_resp_send(req, (const char *)fb->buf, fb->len);
    }
    
    esp_camera_fb_return(fb);
    //int64_t fr_end = esp_timer_get_time();
    //Serial.printf("JPG: %uKB %ums", (uint32_t)(fb_len / 1024), (uint32_t)((fr_end - fr_start) / 1000));
    //Serial.println();
    return res;
}

void startCameraServer(){
  httpd_config_t httpdConfig = HTTPD_DEFAULT_CONFIG();
  //httpdConfig.server_port = 80;

  httpd_uri_t index_uri = {
      .uri = "/",
      .method = HTTP_GET,
      .handler = jpg_httpd_handler,
      .user_ctx = NULL};

  if (httpd_start(&s_httpd, &httpdConfig) == ESP_OK)
  {
    Serial.println("successfully started httpd server");
    if (httpd_register_uri_handler(s_httpd, &index_uri) == ESP_OK)
    {
      Serial.println("successfully attached handler");
    }
  }
}

void initCamera()
{
    camera_config_t config;
    config.ledc_channel = LEDC_CHANNEL_0;
    config.ledc_timer = LEDC_TIMER_0;
    config.pin_d0 = Y2_GPIO_NUM;
    config.pin_d1 = Y3_GPIO_NUM;
    config.pin_d2 = Y4_GPIO_NUM;
    config.pin_d3 = Y5_GPIO_NUM;
    config.pin_d4 = Y6_GPIO_NUM;
    config.pin_d5 = Y7_GPIO_NUM;
    config.pin_d6 = Y8_GPIO_NUM;
    config.pin_d7 = Y9_GPIO_NUM;
    config.pin_xclk = XCLK_GPIO_NUM;
    config.pin_pclk = PCLK_GPIO_NUM;
    config.pin_vsync = VSYNC_GPIO_NUM;
    config.pin_href = HREF_GPIO_NUM;
    config.pin_sscb_sda = SIOD_GPIO_NUM;
    config.pin_sscb_scl = SIOC_GPIO_NUM;
    config.pin_pwdn = PWDN_GPIO_NUM;
    config.pin_reset = RESET_GPIO_NUM;
    config.xclk_freq_hz = 20000000;
    config.pixel_format = PIXFORMAT_JPEG; 
    
    config.frame_size = FRAMESIZE_QQVGA; //FRAMESIZE_SVGA
    config.jpeg_quality = 40;
    config.fb_count = 1;

    // Camera init
    esp_err_t err = esp_camera_init(&config);
    if (err != ESP_OK) {
        Serial.printf("Camera init failed with error 0x%x", err);
        return;
    }

    sensor_t *s = esp_camera_sensor_get();
    s->set_framesize(s, FRAMESIZE_QQVGA);
    s->set_quality(s, 10);
}