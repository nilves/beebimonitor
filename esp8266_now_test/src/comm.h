#include <cppQueue.h>

//ESP NOW packet size
#define PACKET_SIZE 250
#define N_PACETS_IN_QUEUE 20

#define N_DATA_BUFFERS_IN_QUEUE 5
// struct to store the malloced memory buffer
struct dataBuff_t{
  void *data;
  size_t size;
};

myStructureWhichRemebersItsSize *someData;
someData.data = malloc(100);  // and check for NULL
someData.size = 100;

class Comm
{
  public:
    Comm();
    bool sendImg(uint8_t * data, size_t size);
    bool sendAudio(uint8_t * data, size_t size);
    void handle_Comm();
    bool hasImg();
    bool hasAudio();
    size_t getImg(uint8_t ** data);
    size_t getAudio(uint8_t ** data);
  private:
    cppQueue	sendQ(PACKET_SIZE, N_PACETS_IN_QUEUE, FIFO);
    cppQueue    recieveQ(PACKET_SIZE, N_PACETS_IN_QUEUE, FIFO);
    cppQueue    audioPtrQ(size_of(dataBuff_t), N_DATA_BUFFERS_IN_QUEUE, FIFO);
    cppQueue    imgPtrQ(size_of(dataBuff_t), N_DATA_BUFFERS_IN_QUEUE, FIFO);
    void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len);
    void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus);
};