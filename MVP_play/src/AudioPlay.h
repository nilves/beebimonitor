/**
 * Audio library for sending and recieving audio
 * 
 */

#ifndef __audioplay_h__
#define __audioplay_h__

#include <Arduino.h>
#include <driver/i2s.h> // for pin config define
#include <functional> // for the function pointer

#define AUDIOI2S            I2S_NUM_1
#define SAMPLE_RATE         8000 // Hz 
#define SAMPLE_BITS         32    // bits


//#define N_AUDIO_BUFFER_SAMPLES 256
#define N_AUDIO_BUFFER_SAMPLES 128

class AudioPlay
{
public:
    typedef std::function<void(uint8_t * payload, size_t length)> AudioSendCallback;
    
    AudioPlay(void);
    ~AudioPlay(void);

    esp_err_t init(i2s_pin_config_t &audio_pin_config);

    esp_err_t start();
    esp_err_t stop();
    esp_err_t playFrames(uint8_t * payload, size_t length);

protected:
    uint32_t decodeBuf[N_AUDIO_BUFFER_SAMPLES];
};

#endif //__audio_h__