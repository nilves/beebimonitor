// See SetupX_Template.h for all options available

// add following line in User_Setup_Select.h
//#include <../../../../src/my_tft_setup.h>        // Custom setup by Nigul'

//#define ILI9163_DRIVER
#define ST7735_DRIVER //works

#define TFT_WIDTH  128
#define TFT_HEIGHT 160

//esp32 can redefine the pins as needed
#define TFT_MISO -1 //Not connected
#define TFT_MOSI GPIO_NUM_26 //SDA
#define TFT_SCLK GPIO_NUM_25 //SCK
#define TFT_CS   GPIO_NUM_12 // CS   Chip select control pin
#define TFT_DC   GPIO_NUM_27 // A0   Data Command control pin
#define TFT_RST  GPIO_NUM_14 //Reset pin (could connect to RST pin)
//#define TFT_RST  -1  // Set TFT_RST to -1 if display RESET is connected to ESP32 board RST

// For ST7735, ST7789 and ILI9341 ONLY, define the colour order IF the blue and red are swapped on your display
// Try ONE option at a time to find the correct colour order for your display

//#define TFT_RGB_ORDER TFT_RGB  // Colour order Red-Green-Blue
//#define TFT_RGB_ORDER TFT_BGR  // Colour order Blue-Green-Red


// If a backlight control signal is available then define the TFT_BL pin in Section 2
// below. The backlight will be turned ON when tft.begin() is called, but the library
// needs to know if the LEDs are ON with the pin HIGH or LOW. If the LEDs are to be
// driven with a PWM signal or turned OFF/ON then this must be handled by the user
// sketch. e.g. with digitalWrite(TFT_BL, LOW);

// #define TFT_BL   32            // LED back-light control pin
// #define TFT_BACKLIGHT_ON HIGH  // Level to turn ON back-light (HIGH or LOW)


#define LOAD_GLCD   // Font 1. Original Adafruit 8 pixel font needs ~1820 bytes in FLASH
//#define LOAD_FONT2  // Font 2. Small 16 pixel high font, needs ~3534 bytes in FLASH, 96 characters
//#define LOAD_FONT4  // Font 4. Medium 26 pixel high font, needs ~5848 bytes in FLASH, 96 characters
//#define LOAD_FONT6  // Font 6. Large 48 pixel font, needs ~2666 bytes in FLASH, only characters 1234567890:-.apm
//#define LOAD_FONT7  // Font 7. 7 segment 48 pixel font, needs ~2438 bytes in FLASH, only characters 1234567890:.
//#define LOAD_FONT8  // Font 8. Large 75 pixel font needs ~3256 bytes in FLASH, only characters 1234567890:-.
//#define LOAD_FONT8N // Font 8. Alternative to Font 8 above, slightly narrower, so 3 digits fit a 160 pixel TFT
//#define LOAD_GFXFF  // FreeFonts. Include access to the 48 Adafruit_GFX free fonts FF1 to FF48 and custom fonts

//#define SMOOTH_FONT


// #define SPI_FREQUENCY  20000000
#define SPI_FREQUENCY  27000000
// #define SPI_FREQUENCY  40000000

#define SPI_TOUCH_FREQUENCY  2500000

/*
if want to configure throug platformio.ini
build_flags =
  -Os
  -DCORE_DEBUG_LEVEL=ARDUHAL_LOG_LEVEL_DEBUG
  -DUSER_SETUP_LOADED=1
  -DILI9163_DRIVER=1
  -DTFT_WIDTH=128
  -DTFT_HEIGHT=160
  -DTFT_MISO=19
  -DTFT_MOSI=23
  -DTFT_SCLK=18
  -DTFT_CS=5
  -DTFT_DC=19
  -DTFT_RST=-1
  -DLOAD_GLCD=1
  -DSPI_FREQUENCY=27000000
*/

// #define SUPPORT_TRANSACTIONS
