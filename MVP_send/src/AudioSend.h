/**
 * Audio library for sending audio
 * 
 */

#ifndef __audiosend_h__
#define __audiosend_h__

#include <Arduino.h>
#include <driver/i2s.h> // for pin config define
#include <functional> // for the function pointer

#define AUDIOI2S            I2S_NUM_1
#define SAMPLE_RATE         8000 // Hz 
#define SAMPLE_BITS         32    // bits


//#define N_AUDIO_BUFFER_SAMPLES 256
#define N_AUDIO_BUFFER_SAMPLES 128

class AudioSend
{
public:
    typedef std::function<void(uint8_t * payload, size_t length)> AudioSendCallback;
    
    AudioSend(void);
    ~AudioSend(void);

    esp_err_t init(i2s_pin_config_t &audio_pin_config);

    void onMicDataAvailable(AudioSendCallback cb);
    esp_err_t start();
    esp_err_t stop();

protected:
    uint32_t micBuf[N_AUDIO_BUFFER_SAMPLES];
    uint8_t encodeBuf[N_AUDIO_BUFFER_SAMPLES];

    AudioSendCallback _cb;

    //i2c event queue
    QueueHandle_t _i2cEventQueue;
    TaskHandle_t _AudioTaskHandle;

    //call with the Audio object as parameter
    static void AudioTask(void *param);

    void runCb(uint8_t * payload, size_t length) {
        if(_cb) {
            _cb(payload, length);
        }
    }
};

#endif //__audio_h__