#include <audio.h>
#include <g711.h>

Audio::Audio()
{
}

esp_err_t Audio::init(i2s_pin_config_t &pin_config){
    esp_err_t err;
    
    Serial.println("debugA");
    
    i2s_config_t i2s_config = {
        .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_RX),
        .sample_rate = SAMPLE_RATE,                         // 16KHz
        .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT, // could only get it to work with 32bits
        .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT, // although the SEL config should be left, it seems to transmit on right
        .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,     // Interrupt level 1
        .dma_buf_count = 6, //16,                           // number of buffers
        .dma_buf_len = N_AUDIO_BUFFER_SAMPLES, //60,                    // 8 samples per buffer (minimum)
        .use_apll = false, 
        .tx_desc_auto_clear = true                   // disable tx when data not available! 
    };

    err = i2s_driver_install(AUDIOI2S, &i2s_config, 8, &_i2cEventQueue);
    if (err != ESP_OK) {
        Serial.printf("Failed installing audio driver: %d\n", err);
        return err;
    }

    Serial.println("Initialized i2s driver successfully");

    err = i2s_set_pin(AUDIOI2S, &pin_config);
    if (err != ESP_OK) {
        Serial.printf("Failed setting audio pin: %d\n", err);
        return err;
    }

    BaseType_t tcErr = xTaskCreate(AudioTask, "readMic", 4096, this, 1, &_AudioTaskHandle);
    if (pdPASS != tcErr)
    {
        Serial.printf("Failed create mic read task: %d\n", tcErr);
        return err;
    }

    //To suspend the task and stop the i2c hardware
    //stop();
    return ESP_OK;
}

esp_err_t Audio::start(){
    vTaskResume(_AudioTaskHandle);
    return i2s_start(AUDIOI2S);
}

esp_err_t Audio::stop(){
    vTaskSuspend(_AudioTaskHandle);
    return i2s_stop(AUDIOI2S);
}

Audio::~Audio() {
    i2s_driver_uninstall(AUDIOI2S);
    vTaskDelete(_AudioTaskHandle);
}


void Audio::onMicDataAvailable(AudioSendCallback cb)
{
    _cb = cb;
}

void Audio::AudioTask(void *param){
    Audio *audio = (Audio *)param;

    //infinite loop of the freertos task
    while (true)
    {
        // wait for some data to be requested
        i2s_event_t evt;
        if (xQueueReceive(audio->_i2cEventQueue, &evt, portMAX_DELAY) == pdPASS)
        {
            if(evt.type == I2S_EVENT_RX_DONE)
            {
                size_t bytes_read = 0;

                esp_err_t res = i2s_read(AUDIOI2S, audio->micBuf, sizeof(audio->micBuf), &bytes_read, portMAX_DELAY); // no timeout

                // read size should be entire dma buffer or N_AUDIO_BUFFER_SAMPLES samples
                // or 4* N_AUDIO_BUFFER_SAMPLES bytes
                Serial.printf("r: %d", bytes_read); 

                if (res == ESP_OK && bytes_read > 0)
                {
                    size_t nsamples_mic = bytes_read/4;
                    for(size_t i = 0; i < nsamples_mic; i++)
                    {
                        //mic data is 24 bits, shift left for 8 to convert from 32 to 24 bit mic raw data
                        //the aencode gets 16bit and outputs 8 bit need to shift additonal 8bits => total of 16 bit shift
                        audio->encodeBuf[i] = ALaw_Encode((int16_t) (audio->micBuf[i] >> 16));
                    }

                    //run the callback function to send the data
                    audio->runCb(audio->encodeBuf, nsamples_mic);
                }
            }
        }        
    }
}


esp_err_t Audio::playFrames(uint8_t * payload, size_t length)
{
    esp_err_t err;

    if(length > N_AUDIO_BUFFER_SAMPLES)
    {
        Serial.println("Audio play to many samples, buffer overflow");
        return ESP_FAIL;
    }

    for(size_t i = 0; i < length; i++)
    {
      //amplifier input is 32 bit need to shift 16 to fill the most significant bites
      decodeBuf[i] = ((int32_t) ALaw_Decode(payload[i])) << 16;
    }

    size_t bytes_written = 0;
    err = i2s_write(AUDIOI2S, decodeBuf, length*4, &bytes_written, portMAX_DELAY);

    Serial.printf("w: %d\n", bytes_written);

    if (bytes_written != length*4 || err != ESP_OK)
    {
        Serial.println("Audio unable to play all samples");
        return ESP_FAIL; 
    }

    return ESP_OK;
}

