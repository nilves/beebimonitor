/**
 * library for generation of pink noise
 * 
 */

// code from:
// https://github.com/bobh/ESP32AudioFramework/blob/master/paex_pink.ino


#ifndef __noise_h__
#define __noise_h__

#include <Arduino.h>
#include <Audio.h>
#include <math.h>

#define PINK_MAX_RANDOM_ROWS   (30)
#define PINK_RANDOM_BITS       (24)
#define PINK_RANDOM_SHIFT      ((sizeof(int32_t)*8)-PINK_RANDOM_BITS)

//the volume scale is logartimic and values are shifted left
#define VOLUME_MIN (4) // should be less than PINK_RANDOM_SHIFT
#define VOLUME_MAX (0)
#define VOLUME_DEFAULT (2)

//use this to print out the min and max values
//#define PINK_MEASURE

class NoiseGenerator
{
public:
    NoiseGenerator(Audio *audio_obj, uint8_t numRows);
    ~NoiseGenerator(void);

    /*todo set volume by shifting the noise parameter
     more or less*/
    void start();
    void stop();

    void set_volume(uint8_t volume);
    void set_volume_up();
    void set_volume_down();
    void set_volume_max();
    void set_volume_min();
    void set_volume_default();
    void set_volume_debug();

protected:
    TaskHandle_t _NoiseTaskHandle;

    //call with the noise object as parameter
    static void NoiseTask(void *noise_obj);

private:
    uint32_t      _Rows[PINK_MAX_RANDOM_ROWS];
    uint32_t      _RunningSum;   /* Used to optimize summing of generators. */
    uint32_t      _Index;        /* Incremented each sample. */
    uint32_t      _IndexMask;    /* Index wrapped by ANDing with this mask. */
    uint16_t      _Scalar;       /* Used to scale within range of 0 to 2^PINK_RANDOM_BITS-1 */

    uint32_t _noisebuffer[N_AUDIO_BUFFER_SAMPLES];
    Audio * _audio_obj;

    //left shifts values to right by 0 (max) or 32 (min)
    uint8_t _volume; 

#ifdef PINK_MEASURE
    uint32_t _max = 0;
    uint32_t _min = 0xFFFFFFFF;
    uint32_t _max_w = 0;
    uint32_t _min_w = 0xFFFFFFFF;
#endif

    uint32_t GeneratePinkNoise();
    uint32_t GenerateWhiteNoise();
    uint32_t GenerateRandomNumber( void );
};

#endif