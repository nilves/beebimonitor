#include <audioplay.h>
#include <g711.h>

AudioPlay::AudioPlay()
{
}

esp_err_t AudioPlay::init(i2s_pin_config_t &pin_config){
    esp_err_t err;
    
    i2s_config_t i2s_config = {
        .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_TX),
        .sample_rate = SAMPLE_RATE,                         // 16KHz
        .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT, // could only get it to work with 32bits
        .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT, // although the SEL config should be left, it seems to transmit on right
        .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,     // Interrupt level 1
        .dma_buf_count = 6, //16,                           // number of buffers
        .dma_buf_len = N_AUDIO_BUFFER_SAMPLES, //60,                    // 8 samples per buffer (minimum)
        .use_apll = false, 
        .tx_desc_auto_clear = true                   // disable tx when data not available! 
    };

    err = i2s_driver_install(AUDIOI2S, &i2s_config, 0, NULL);
    if (err != ESP_OK) {
        Serial.printf("Failed installing audio driver: %d\n", err);
        return err;
    }

    Serial.println("Initialized i2s driver successfully");

    err = i2s_set_pin(AUDIOI2S, &pin_config);
    if (err != ESP_OK) {
        Serial.printf("Failed setting audio pin: %d\n", err);
        return err;
    }

    //stop the i2c hardware start when websocket is connected
    stop();
    return ESP_OK;
}

esp_err_t AudioPlay::start(){
    i2s_zero_dma_buffer(AUDIOI2S); //hope this helps against the hacking of output?
    return i2s_start(AUDIOI2S);
    
}

esp_err_t AudioPlay::stop(){
    return i2s_stop(AUDIOI2S);
}

AudioPlay::~AudioPlay() {
    i2s_driver_uninstall(AUDIOI2S);
}

esp_err_t AudioPlay::playFrames(uint8_t * payload, size_t length)
{
    esp_err_t err;

    if(length > N_AUDIO_BUFFER_SAMPLES)
    {
        Serial.println("Audio play to many samples, buffer overflow");
        return ESP_FAIL;
    }

    for(size_t i = 0; i < length; i++)
    {
      //Serial.println(ALaw_Decode(payload[i]));
      //Serial.println((int32_t) ALaw_Decode(payload[i]));
      
      //Serial.println((int32_t) ALaw_Decode(payload[i]) << 16);
      
      //amplifier input is 32 bit need to shift 16 to fill the most significant bites
      decodeBuf[i] = ((int32_t) ALaw_Decode(payload[i])) << 16;
    }

    size_t bytes_written = 0;
    err = i2s_write(AUDIOI2S, decodeBuf, length*4, &bytes_written, portMAX_DELAY);

    //Serial.printf("w: %d\n", bytes_written);

    if (bytes_written != length*4 || err != ESP_OK)
    {
        Serial.println("Audio unable to play all samples");
        return ESP_FAIL; 
    }

    return ESP_OK;
}

