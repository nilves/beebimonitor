#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <espnow.h>

// https://randomnerdtutorials.com/esp-now-esp8266-nodemcu-arduino-ide/

// esp8266 mac address EC:FA:BC:0E:B6:55
uint8_t broadcastAddress1[] = {0xEC, 0xFA, 0xBC, 0x0E, 0xB6, 0x55};
// espcam mac address 7C:9E:BD:38:40:D8
uint8_t broadcastAddress2[] = {0x7C, 0x9E, 0xBD, 0x38, 0x40, 0xD8};


// Callback function that will be executed when data is received
void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) {
  //memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("data:\"");
  Serial.print((char*) incomingData);
  Serial.println("\"");
}

// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  Serial.print("Last Packet Send Status: ");
  if (sendStatus == 0){
    Serial.println("Delivery success");
  }
  else{
    Serial.println("Delivery fail");
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  
  Serial.println("MAC address:");
  Serial.println(WiFi.macAddress());

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  esp_now_register_recv_cb(OnDataRecv);

  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_add_peer(broadcastAddress2, ESP_NOW_ROLE_COMBO, 1, NULL, 0);

}

void loop() {
  // put your main code here, to run repeatedly:

  // Send message via ESP-NOW
  char tmp_data[] = "Hello world! from esp8266.";

  int result = esp_now_send(broadcastAddress2, (uint8_t *) &tmp_data, sizeof(tmp_data));

  if (result == 0) {
    Serial.println("Sent with success");
  }
  else {
    Serial.println("Error sending the data");
  }

  delay(1000);
}