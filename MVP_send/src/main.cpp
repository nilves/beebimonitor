#include <Arduino.h>
#include <WiFi.h>

#include <WebSocketsServer.h>
#include <AudioSend.h>

const char* ssid     = "tuduroam";
const char* password = "issitest1337"; //must be at least 8 char

#define LED_PIN GPIO_NUM_14

AudioSend audio = AudioSend();

WebSocketsServer webSocket = WebSocketsServer(81);

//send the audiodata data over websocket
void sendAudio(uint8_t * payload, size_t length)
{
  //Serial.print("#");
  webSocket.broadcastBIN(payload, length);
  //webSocket.sendBIN(payload, length);

  //led toggle
  digitalWrite(LED_PIN, !digitalRead(LED_PIN)); 
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            Serial.printf("[%u] Disconnected!\n", num);
            // test if no clients then stop audio sending
            if(0 == webSocket.connectedClients(false))
            {
              audio.stop();
              digitalWrite(LED_PIN, HIGH); //led off
            }
            break;
        case WStype_CONNECTED:
            {
                IPAddress ip = webSocket.remoteIP(num);
                Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
            }
            //this may be run multiple times if multiple recievers are connected
            audio.start();
            break;
        case WStype_TEXT:
        case WStype_BIN:
        case WStype_ERROR:			
        case WStype_FRAGMENT_TEXT_START:
        case WStype_FRAGMENT_BIN_START:
        case WStype_FRAGMENT:
        case WStype_FRAGMENT_FIN:
        case WStype_PING:
        case WStype_PONG:
          break;
    }

}

//audio stuff
// The pin config as per the setup
i2s_pin_config_t audio_pin_config = {
    .bck_io_num = GPIO_NUM_13,   // BCKL SCK
    .ws_io_num = GPIO_NUM_15,    // LRCL WS
    .data_out_num = I2S_PIN_NO_CHANGE, // amp (only for speakers)
    .data_in_num = GPIO_NUM_12   // microfon
};


void setup() {
  Serial.begin(115200);
  
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH); //led off

  Serial.print("Setting soft-AP ... ");
  boolean result = WiFi.softAP(ssid, password);
  if(result == true)
  {
    Serial.println("Ready");
  }
  else
  {
    Serial.println("Failed!");
  }

  if(!WiFi.softAPConfig(IPAddress(192, 168, 1, 11), IPAddress(192, 168, 1, 1), IPAddress(255, 255, 255, 0))){
      Serial.println("AP Config Failed");
  }

  Serial.println("AP IP address: ");
  Serial.println(WiFi.softAPIP());

  //start the websocket
  Serial.println("Starting websocket server...");
  webSocket.begin();
  webSocket.onEvent(webSocketEvent); //define in the cmd.h

  //init the audio object
  Serial.println("Starting audio send module ... ");
  audio.init(audio_pin_config);
  audio.onMicDataAvailable(sendAudio);
}

void loop() {
  // put your main code here, to run repeatedly:
  webSocket.loop();

  /*
  if (WiFi.softAPgetStationNum() > 0)
  {
    Serial.println("A player connected to wifi");
  }
  */
}