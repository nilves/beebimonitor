#include <Noise.h>
/* 
   This program uses the PortAudio Portable Audio Library.
   For more information see: http://www.portaudio.com
   Copyright (c) 1999-2000 Ross Bencina and Phil Burk
   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files
   (the "Software"), to deal in the Software without restriction,
   including without limitation the rights to use, copy, modify, merge,
   publish, distribute, sublicense, and/or sell copies of the Software,
   and to permit persons to whom the Software is furnished to do so,
   subject to the following conditions:
   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
   ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
   CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
   The text above constitutes the entire PortAudio license; however,
   the PortAudio community also makes the following non-binding requests:
   Any person wishing to distribute modifications to the Software is
   requested to send the modifications to the original developer so that
   they can be incorporated into the canonical version. It is also
   requested that these non-binding requests be included along with the
   license above.
*/


// This code ported from http://portaudio.com and modified by Nigul Ilves

NoiseGenerator::NoiseGenerator(Audio *audio_obj, uint8_t numRows)
{
  _audio_obj = audio_obj;

  //noise parameters restart
  _Index = 0;

  if(numRows > PINK_MAX_RANDOM_ROWS)
  {
    numRows = PINK_MAX_RANDOM_ROWS;
  }
  _IndexMask = (1 << numRows) - 1;

  // calculate Scalar do divide the output with to get a noise with right bit length
  _Scalar = (numRows + 1);

  // Initialize rows.
  for (uint8_t i = 0; i < numRows; i++ )
  {
    _Rows[i] = 0;
  }

  _RunningSum = 0;

  _volume = VOLUME_DEFAULT;

  BaseType_t tcErr = xTaskCreate(NoiseTask, "noise", 4096, this, 1, &_NoiseTaskHandle);
    if (pdPASS != tcErr)
    {
        Serial.printf("Failed create pink noise task: %d\n", tcErr);
        return;
    }

    stop();
}

void NoiseGenerator::start(){
    Serial.println("Start noise");
    vTaskResume(_NoiseTaskHandle);
}

void NoiseGenerator::stop(){
    Serial.println("Stop noise");
    vTaskSuspend(_NoiseTaskHandle);
}

NoiseGenerator::~NoiseGenerator() {
    vTaskDelete(_NoiseTaskHandle);
}


void NoiseGenerator::NoiseTask(void *noise_obj){
    NoiseGenerator *noise = (NoiseGenerator *)noise_obj;
    uint8_t shift; 
    //infinite loop of the freertos task
    while (true)
    {
        shift = PINK_RANDOM_SHIFT - noise->_volume;
        
        //generate noise frame
        for(uint8_t i = 0; i < N_AUDIO_BUFFER_SAMPLES; i++)
        {
          noise->_noisebuffer[i] = noise->GeneratePinkNoise() << shift;
          // noise->_noisebuffer[i] = noise->GenerateWhiteNoise();
        }

#ifdef PINK_MEASURE
        Serial.printf("min %#010x, max %#010x\n", noise->_min << PINK_RANDOM_SHIFT, noise->_max << PINK_RANDOM_SHIFT);
        Serial.printf("min_W %#010x, max_W %#010x\n", noise->_min_w, noise->_max_w);
#endif

        //send the frame
        noise->_audio_obj->play((uint8_t*)noise->_noisebuffer, \
            N_AUDIO_BUFFER_SAMPLES * sizeof(uint32_t));
    }
}

uint32_t NoiseGenerator::GenerateWhiteNoise()
{
  return GenerateRandomNumber();
}

/* Generate Pink noise values between 0 and 2^PINK_RANDOM_BITS-1*/
uint32_t NoiseGenerator::GeneratePinkNoise()
{
  long newRandom;
  long sum;
  float output;


  // Increment and mask index.
  _Index = (_Index + 1) & _IndexMask;
  // If index is zero, don't update any random values.

  if ( _Index != 0 )
  {
    // Determine how many trailing zeros in PinkIndex.
    // This algorithm will hang if n==0 so test first.
    int numZeros = 0;
    int n = _Index;

    while ( (n & 1) == 0 )
    {
      n = n >> 1;
      numZeros++;
    }

    // Replace the indexed ROWS random value.
    // Subtract and add back to RunningSum instead of adding all the random
    // values together. Only one changes each time.

    _RunningSum -= _Rows[numZeros];
    newRandom = ((int32_t)GenerateRandomNumber()) >> PINK_RANDOM_SHIFT;
    _RunningSum += newRandom;
    _Rows[numZeros] = newRandom;

  }

  // Add extra white noise value.
  newRandom = ((int32_t)GenerateRandomNumber()) >> PINK_RANDOM_SHIFT;
  sum = _RunningSum + newRandom;
  // Scale to range of 0 to 2^PINK_RANDOM_BITS-1.
  output = sum / _Scalar;

#ifdef PINK_MEASURE
  // Check Min/Max
  if ( output > _max ) _max = output;
  else if ( output < _min ) _min = output;
#endif

  return output;
}

/************************************************************/
/* Calculate pseudo-random 32 bit number based on linear congruential method. */
uint32_t NoiseGenerator::GenerateRandomNumber( void )
{
  /* Change this seed for different random sequences. */
  static uint32_t randSeed = 22222;
  //Serial.printf("randseedh: %u\n", randSeed);
  randSeed = (randSeed * 196314165) + 907633515;

#ifdef PINK_MEASURE
  // Check Min/Max
  if ( randSeed > _max_w ) _max_w = randSeed;
  else if ( randSeed < _min_w ) _min_w = randSeed;
#endif

  return randSeed;
}

void NoiseGenerator::set_volume(uint8_t volume){
  if (volume > VOLUME_MIN) //note that the internal volume is inverted
  {
    volume = VOLUME_MIN;
  }
  _volume = VOLUME_MIN - volume;
}
void NoiseGenerator::set_volume_up(){
  if(_volume-- == VOLUME_MAX)
  {
    _volume = VOLUME_MAX; 
  }
}

void NoiseGenerator::set_volume_down(){
  if(++_volume > VOLUME_MIN)
  {
    _volume = VOLUME_MIN; 
  }
}

void NoiseGenerator::set_volume_max()
{
  _volume = VOLUME_MAX;
}

void NoiseGenerator::set_volume_min()
{
  _volume = VOLUME_MIN; 
}

void NoiseGenerator::set_volume_default()
{
  _volume = VOLUME_DEFAULT; 
}

void NoiseGenerator::set_volume_debug()
{
  Serial.printf("noise volume: %d\n", _volume); 
}

        