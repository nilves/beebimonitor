#include <Arduino.h>
#include <WiFi.h>
//#include <WebServer.h>
#include <WebSocketsServer.h>

#include "soc/soc.h" //disable brownout problems
#include "soc/rtc_cntl_reg.h"  //disable brownout problems

#include <driver/i2s.h>
#include <Audio.h>
#include <Noise.h>
#include <cmd.h>

#include "Camera_srv.h"



// Replace with your network credentials
const char* ssid     = "OTTO";
const char* password = "testtest123";

//const char* ssid = "Telia-A0FDC7";
//const char* password = "CFFH7KMUVS";

// Set your Static IP address
IPAddress local_IP(192, 168, 1, 184);
// Set your Gateway IP address
IPAddress gateway(192, 168, 1, 1);

IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional

WebSocketsServer webSocket = WebSocketsServer(81);

//audio stuff
// The pin config as per the setup
i2s_pin_config_t audio_pin_config = {
    .bck_io_num = GPIO_NUM_13,   // BCKL SCK
    .ws_io_num = GPIO_NUM_15,    // LRCL WS
    .data_out_num = GPIO_NUM_2, // amp (only for speakers)
    .data_in_num = GPIO_NUM_14   // microfon
};

void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.println("Connected to AP successfully!");
}

void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(info.disconnected.reason);
  Serial.println("Trying to Reconnect");
  WiFi.reconnect();
}

Audio *audio;
NoiseGenerator *noise;

void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
 
  Serial.begin(115200);
  //Serial.setDebugOutput(false);

  //init the built in led
  pinMode(BUILT_IN_LED, OUTPUT);

  initCamera();

  WiFi.onEvent(WiFiStationConnected, SYSTEM_EVENT_STA_CONNECTED);
  WiFi.onEvent(WiFiGotIP, SYSTEM_EVENT_STA_GOT_IP);
  WiFi.onEvent(WiFiStationDisconnected, SYSTEM_EVENT_STA_DISCONNECTED);

  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }
  
  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // todo move this to the connected event?
  // Start streaming web server
  startCameraServer();

  //register the commands for websocket
  registerCmd();
  //start the websocket
  webSocket.begin();
  webSocket.onEvent(webSocketEvent); //define in the cmd.h

  audio = new Audio();
  audio->init(audio_pin_config);
  noise = new NoiseGenerator(audio, 20);
  audio->start();
  noise->stop();
}

void loop(){
  //Todo add this websocket to a freertos task
  webSocket.loop();

  //Todo add reconnect https://randomnerdtutorials.com/solved-reconnect-esp32-to-wifi/
  
}
