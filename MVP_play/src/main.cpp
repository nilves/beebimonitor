/**
 * This is MVP of baby monitor parent side which 
 * connects to a certain wifi AP and websocket client
 * and just plays any data sent over websocket.
 */

#include <Arduino.h>
#include <WiFi.h>
#include <AudioPlay.h>
#include <WebSocketsClient.h>

const char* ssid     = "tuduroam";
const char* password = "issitest1337"; //must be at least 8 char

void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.println("Connected to AP successfully!");
}

void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info){
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(info.disconnected.reason);
  Serial.println("Trying to Reconnect");
  while(!WiFi.reconnect())
  {
    delay(500);
    Serial.print(".");
  }
}

WebSocketsClient webSocket;

i2s_pin_config_t audio_pin_config = {
    .bck_io_num = GPIO_NUM_13,   // BCKL SCK
    .ws_io_num = GPIO_NUM_15,    // LRCL WS
    .data_out_num = GPIO_NUM_12, // amp (only for speakers)
    .data_in_num = I2S_PIN_NO_CHANGE   // microfon
};

#define LED_PIN GPIO_NUM_14

AudioPlay audio;

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

	switch(type) {
		case WStype_CONNECTED:
			Serial.printf("[WSc] Connected to url: %s\n", payload);
      audio.start();
			break;
    case WStype_DISCONNECTED:
			Serial.printf("[WSc] Disconnected!\n"); 
      audio.stop();
      
      digitalWrite(LED_PIN, HIGH); //led off
      
			break;
		case WStype_BIN:
			//Serial.printf("[WSc] get binary length: %u\n", length);
			audio.playFrames(payload, length);
      
      //led toggle
      digitalWrite(LED_PIN, !digitalRead(LED_PIN)); 

			break;
    case WStype_TEXT:
		case WStype_ERROR:			
		case WStype_FRAGMENT_TEXT_START:
		case WStype_FRAGMENT_BIN_START:
		case WStype_FRAGMENT:
		case WStype_FRAGMENT_FIN:
    case WStype_PING:
    case WStype_PONG:
			break;
	}

}

void setup() {
  Serial.begin(115200);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH); //led off

  //setup wifi station
  WiFi.mode(WIFI_STA);
  WiFi.onEvent(WiFiStationConnected, SYSTEM_EVENT_STA_CONNECTED);
  WiFi.onEvent(WiFiGotIP, SYSTEM_EVENT_STA_GOT_IP);
  WiFi.onEvent(WiFiStationDisconnected, SYSTEM_EVENT_STA_DISCONNECTED);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  // wait for connection and check status
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //connect the websocket
	webSocket.begin("192.168.1.11", 81, "/");
	webSocket.onEvent(webSocketEvent);
	//default is 500ms 
  //webSocket.setReconnectInterval(1000);

  //create the audio object
  Serial.println("init audio");
  audio.init(audio_pin_config);
}

void loop() {
  //Todo add this websocket to a freertos task
  webSocket.loop();
}