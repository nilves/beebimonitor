#include <Arduino.h>
#include <SPI.h>
#include <TFT_eSPI.h>
#include <WiFi.h>
#include "esp_http_client.h"

#include <Audio.h>

#include <WebSocketsClient.h>
WebSocketsClient webSocket;


// The pin config as per the setup
i2s_pin_config_t audio_pin_config = {
    .bck_io_num = GPIO_NUM_0,   // BCKL SCK
    .ws_io_num = GPIO_NUM_4,    // LRCL WS
    .data_out_num = GPIO_NUM_17, // amp (only for speakers)
    .data_in_num = GPIO_NUM_16   // microfon
};

Audio *audio;

/*
void loopback(uint8_t * payload, size_t length)
{
  audio->playFrames(payload, length);
}
*/

//send the audiodata data over websocket
void sendAudio(uint8_t * payload, size_t length)
{
  webSocket.sendBIN(payload, length);
}


//ILI9163C
TFT_eSPI tft = TFT_eSPI();

// JPEG decoder library
#include <JPEGDecoder.h>

// Return the minimum of two values a and b
#define minimum(a,b)     (((a) < (b)) ? (a) : (b))

//function declarations
void drawArrayJpeg(const uint8_t arrayname[], uint32_t array_size, int xpos, int ypos);
void renderJPEG(int xpos, int ypos);
void jpegInfo();
void showTime(uint32_t msTime);
//----------------------------------------------------------------------------------------------------
void displayNextFrame();

const char* ssid     = "OTTO";
const char* password = "testtest123"; //must be at least 8 char


void hexdump(const void *mem, uint32_t len, uint8_t cols = 16) {
	const uint8_t* src = (const uint8_t*) mem;
	Serial.printf("\n[HEXDUMP] Address: 0x%08X len: 0x%X (%d)", (ptrdiff_t)src, len, len);
	for(uint32_t i = 0; i < len; i++) {
		if(i % cols == 0) {
			Serial.printf("\n[0x%08X] 0x%08X: ", (ptrdiff_t)src, i);
		}
		Serial.printf("%02X ", *src);
		src++;
	}
	Serial.printf("\n");
}

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

	switch(type) {
		case WStype_DISCONNECTED:
			Serial.printf("[WSc] Disconnected!\n");
			break;
		case WStype_CONNECTED:
			Serial.printf("[WSc] Connected to url: %s\n", payload);

			// send message to server when Connected
			webSocket.sendTXT("Connected");
      audio->start();
			break;
		case WStype_TEXT:
			Serial.printf("[WSc] get text: %s\n", payload);

			// send message to server
			// webSocket.sendTXT("message here");
			break;
		case WStype_BIN:
			Serial.printf("[WSc] get binary length: %u\n", length);
			audio->playFrames(payload, length);
      //hexdump(payload, length);

			// send data to server
			// webSocket.sendBIN(payload, length);
			break;
		case WStype_ERROR:			
		case WStype_FRAGMENT_TEXT_START:
		case WStype_FRAGMENT_BIN_START:
		case WStype_FRAGMENT:
		case WStype_FRAGMENT_FIN:
    case WStype_PING:
    case WStype_PONG:
			break;
	}

}


void setup() {
  Serial.begin(115200);
  Serial.println();

  tft.begin();
  tft.fillScreen(TFT_BLACK);

  Serial.print("Setting soft-AP ... ");
  boolean result = WiFi.softAP(ssid, password);
  if(result == true)
  {
    Serial.println("Ready");
  }
  else
  {
    Serial.println("Failed!");
  }

  if(!WiFi.softAPConfig(IPAddress(192, 168, 1, 1), IPAddress(192, 168, 1, 1), IPAddress(255, 255, 255, 0))){
      Serial.println("AP Config Failed");
  }

  // server address, port and URL
	webSocket.begin("192.168.1.184", 81, "/");

	// event handler
	webSocket.onEvent(webSocketEvent);

	// use HTTP Basic Authorization this is optional remove if not needed
	//webSocket.setAuthorization("user", "Password");

	// try ever 1000ms again if connection has failed
	webSocket.setReconnectInterval(1000);

  Serial.println("Create audio object");
  audio = new Audio();
  Serial.println("init audio object");
  audio->init(audio_pin_config);
  audio->onMicDataAvailable(sendAudio);
  audio->stop();
}

void loop() {
  webSocket.loop();

  if (WiFi.softAPgetStationNum() > 0)
  {
      if(webSocket.isConnected())
      {
        Serial.println("websocket send lamp on txt");
        webSocket.sendTXT("RecordAlways");
        webSocket.sendTXT("PlayOff");
        webSocket.sendTXT("LampOn");
        webSocket.loop();

        delay(1000);

        Serial.println("websocket send lamp off txt");
        webSocket.sendTXT("PlayNoise");
        webSocket.sendTXT("LampOff");
        webSocket.loop();

        delay(1000);
      }
      //debug disable the video to start with
      //displayNextFrame();
      
  }
}

//####################################################################################################
// Draw a JPEG on the TFT pulled from a program memory array
//####################################################################################################
void drawArrayJpeg(const uint8_t arrayname[], uint32_t array_size, int xpos, int ypos) {

  int x = xpos;
  int y = ypos;

  JpegDec.decodeArray(arrayname, array_size);
  
  //jpegInfo(); // Print information from the JPEG file (could comment this line out)
  
  renderJPEG(x, y);
  
  //Serial.println("#########################");
}

//####################################################################################################
// Draw a JPEG on the TFT, images will be cropped on the right/bottom sides if they do not fit
//####################################################################################################
// This function assumes xpos,ypos is a valid screen coordinate. For convenience images that do not
// fit totally on the screen are cropped to the nearest MCU size and may leave right/bottom borders.
void renderJPEG(int xpos, int ypos) {

  // retrieve infomration about the image
  uint16_t *pImg;
  uint16_t mcu_w = JpegDec.MCUWidth;
  uint16_t mcu_h = JpegDec.MCUHeight;
  uint32_t max_x = JpegDec.width;
  uint32_t max_y = JpegDec.height;

  // Jpeg images are draw as a set of image block (tiles) called Minimum Coding Units (MCUs)
  // Typically these MCUs are 16x16 pixel blocks
  // Determine the width and height of the right and bottom edge image blocks
  uint32_t min_w = minimum(mcu_w, max_x % mcu_w);
  uint32_t min_h = minimum(mcu_h, max_y % mcu_h);

  // save the current image block size
  uint32_t win_w = mcu_w;
  uint32_t win_h = mcu_h;

  // record the current time so we can measure how long it takes to draw an image
  uint32_t drawTime = millis();

  // save the coordinate of the right and bottom edges to assist image cropping
  // to the screen size
  max_x += xpos;
  max_y += ypos;

  // read each MCU block until there are no more
  while (JpegDec.readSwappedBytes()) {
	  
    // save a pointer to the image block
    pImg = JpegDec.pImage ;

    // calculate where the image block should be drawn on the screen
    int mcu_x = JpegDec.MCUx * mcu_w + xpos;  // Calculate coordinates of top left corner of current MCU
    int mcu_y = JpegDec.MCUy * mcu_h + ypos;

    // check if the image block size needs to be changed for the right edge
    if (mcu_x + mcu_w <= max_x) win_w = mcu_w;
    else win_w = min_w;

    // check if the image block size needs to be changed for the bottom edge
    if (mcu_y + mcu_h <= max_y) win_h = mcu_h;
    else win_h = min_h;

    // copy pixels into a contiguous block
    if (win_w != mcu_w)
    {
      uint16_t *cImg;
      int p = 0;
      cImg = pImg + win_w;
      for (int h = 1; h < win_h; h++)
      {
        p += mcu_w;
        for (int w = 0; w < win_w; w++)
        {
          *cImg = *(pImg + w + p);
          cImg++;
        }
      }
    }

    // draw image MCU block only if it will fit on the screen
    if (( mcu_x + win_w ) <= tft.width() && ( mcu_y + win_h ) <= tft.height())
    {
      tft.pushRect(mcu_x, mcu_y, win_w, win_h, pImg);
    }
    else if ( (mcu_y + win_h) >= tft.height()) JpegDec.abort(); // Image has run off bottom of screen so abort decoding
  }

  // calculate how long it took to draw the image
  drawTime = millis() - drawTime;

  // print the results to the serial port
  Serial.print(F(  "Total render time was    : ")); Serial.print(drawTime); Serial.println(F(" ms"));
  Serial.println(F(""));
}


const int kMaxJpegFileSize = 3072; //Todo calculate real size needed for qqvga 120*160*2 16 bit color value 23kb? => nope around 1kbyte
char pBuf[kMaxJpegFileSize];

void displayNextFrame()
{
  esp_http_client_config_t config = {
      .url = "http://192.168.1.184/",
  };

  esp_http_client_handle_t client = esp_http_client_init(&config);

  esp_http_client_open(client, 0);
  int content_length = esp_http_client_fetch_headers(client);
  int read_len = 0;
  if (content_length > 0 && content_length <= sizeof(pBuf))
  {
    read_len = esp_http_client_read(client, pBuf, content_length);
  }
  esp_http_client_close(client);
  esp_http_client_cleanup(client);

  if (read_len){
    tft.setRotation(1);  // landscape
    drawArrayJpeg((uint8_t *)pBuf, read_len, 0, 0); // Draw a jpeg image stored in memory
  }
}
