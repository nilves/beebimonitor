#include <Arduino.h>
#include <driver/i2s.h>
#include <Audio.h>

// The pin config as per the setup
i2s_pin_config_t audio_pin_config = {
    .bck_io_num = GPIO_NUM_0,   // BCKL SCK
    .ws_io_num = GPIO_NUM_4,    // LRCL WS
    .data_out_num = GPIO_NUM_17, // amp (only for speakers)
    .data_in_num = GPIO_NUM_16   // microfon
};

Audio *audio;

void loopback(uint8_t * payload, size_t length)
{
  audio->playFrames(payload, length);
}

void setup()
{
  Serial.begin(115200);

  Serial.println("Create audio object");
  audio = new Audio();
  Serial.println("init audio object");
  audio->init(audio_pin_config);
  audio->onMicDataAvailable(loopback);
}

void loop()
{
  // nothing to do here - everything is taken care of by tasks
}

